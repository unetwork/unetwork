;;; UNETWORK library
;;; Basic features for the SBCL implementation
;;;
;;; Copyright (C) 2003-2004  Matthieu Villeneuve (matthieu.villeneuve@free.fr)
;;;
;;; The author grants you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(in-package :unetwork)

(defun resolve-host-ip (hostname)
  "Return the IP address of a host."
  (let ((host (sb-bsd-sockets:get-host-by-name hostname)))
    (when (null host)
      (error 'unknown-host-error :host hostname))
    (sb-bsd-sockets:host-ent-address host)))

(defun open-socket (host port &key (type :text))
  "Open a socket on specified host and port. Keyword argument TYPE
can be either :TEXT or :BINARY (defaults to :TEXT)."
  (handler-case
      (let ((host-address (resolve-host-ip host))
            (sock (sb-bsd-sockets:make-inet-socket :stream :tcp)))
        (sb-bsd-sockets:socket-connect sock host-address port)
        (let ((stream (sb-bsd-sockets:socket-make-stream
                       sock
                       :input t :output t
                       :element-type (if (eq type :text)
                                         'character
                                         '(unsigned-byte 8)))))
          (make-instance 'socket :sock sock :stream stream)))
    (simple-error () (error 'connection-error :host host))))
  
(defun close-socket (socket)
  "Close a socket."
  (sb-bsd-sockets:socket-close (socket-sock socket)))

(defun open-server-socket (port)
  "Open a server socket on localhost on specified port."
  (error "Not implemented yet."))

(defun server-socket-accept (server-socket &key timeout)
  "Accept a connection on a server socket. Return the
resulting socket."
  (error "Not implemented yet."))

(defun close-server-socket (server-socket)
  "Close a server socket."
  (error "Not implemented yet."))
