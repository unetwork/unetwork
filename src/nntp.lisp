;;; UNETWORK library
;;; NNTP protocol
;;;
;;; Copyright (C) 2003-2004  Matthieu Villeneuve (matthieu.villeneuve@free.fr)
;;;
;;; The author grants you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(in-package :unetwork)

(unless (boundp '+nntp-default-port+)
  (defconstant +nntp-default-port+ 119))

(defun nntp-open-connection (server user password
                             &optional (port +nntp-default-port+))
  "Open a connection to a NNTP server. Returns the connection socket."
  (let ((socket (open-socket server port :type :text)))
    (unless (null user)
      (nntp-handle-command socket "AUTHINFO USER" (list user)
                           :expect '("281" "381")))
    (unless (null password)
      (nntp-handle-command socket "AUTHINFO PASS" (list password)
                           :expect '("281")))
    socket))

(defun nntp-close-connection (socket)
  "Close a connection to a NNTP server."
  (let ((stream (socket-stream socket)))
    (format stream "CLOSE~%"))
  (close-socket socket))

(defun nntp-get-groups (socket)
  "Get the list of all groups on the server. Returns a list of lists
of four elements: group name, first and last message indices,
posting allowed?"
  (nntp-handle-command socket "LIST" nil :expect '("215"))
  (nntp-read-group-list socket))

(defun nntp-get-group-info (socket group)
  "Get information about a group. Returns three values: first and
last message index, estimate of number of messages."
  (multiple-value-bind (status info)
      (nntp-handle-command socket "GROUP" (list group) :expect '("211"))
    (declare (ignore status))
    (values-list (mapcar #'parse-integer (split-string info #\Space)))))

(defun nntp-get-article (socket article-id)
  "Get an article. Returns a DOCUMENT object."
  (nntp-handle-command socket "ARTICLE" (list article-id) :expect '("220"))
  (let ((document (make-instance 'document)))
    (document-read-properties document socket)
    (document-read-dot-terminated-text document socket)
    document))

(defun nntp-handle-command (socket command params &key expect)
  (handle-simple-command "NNTP" socket command params :ok-status expect))

(defun nntp-read-group-list (socket)
  (let ((stream (socket-stream socket)))
    (loop for line = (trim-line (read-line stream nil nil))
          until (or (null line) (string= line "."))
          collect (let ((tokens (split-string line #\Space)))
                    (list (first tokens)
                          (parse-integer (second tokens))
                          (parse-integer (third tokens))
                          (string= (fourth tokens) "y"))))))
