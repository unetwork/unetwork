;;; UNETWORK library
;;; URI streams
;;;
;;; Copyright (C) 2003-2004  Matthieu Villeneuve (matthieu.villeneuve@free.fr)
;;;
;;; The author grants you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(in-package :unetwork)

(defparameter *uri-input-stream-handlers* '())

(defmacro with-uri-input-stream ((stream uri-string &optional status properties)
                                 &body body)
  "Opens an input stream to the resource at a given uri. Initial connection
status and resource properties are bound to the variables STATUS and
PROPERTIES, if provided. Evaluates the BODY forms in an implicit PROGN,
then closes the stream."
  (with-gensyms (uri socket connection)
    `(let* ((,uri (parse-uri ,uri-string))
            (,connection (open-connection ,uri))
            (,(or status (gensym)) (connection-status ,connection))
            (,(or properties (gensym)) (connection-properties ,connection))
            (,socket (connection-socket ,connection)))
      (unwind-protect
           (let ((,stream (socket-stream ,socket)))
             ,@body)
        (close-connection ,connection)))))
