;;; UNETWORK library
;;; Basic features for the CLISP implementation
;;;
;;; Copyright (C) 2003-2004  Matthieu Villeneuve (matthieu.villeneuve@free.fr)
;;;
;;; The author grants you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(in-package :unetwork)

(defun resolve-host-ip (hostname)
  "Return the IP address of a host."
  (error "Not implemented yet."))

(defun open-socket (host port &key (type :text))
  "Open a socket on specified host and port. Keyword argument TYPE
can be either :TEXT or :BINARY (defaults to :TEXT)."
  (error "Not implemented yet."))
  
(defun close-socket (socket)
  "Close a socket."
  (error "Not implemented yet."))

(defun open-server-socket (port)
  "Open a server socket on localhost on specified port."
  (error "Not implemented yet."))

(defun server-socket-accept (server-socket &key timeout)
  "Accept a connection on a server socket. Return the
resulting socket."
  (error "Not implemented yet."))

(defun close-server-socket (server-socket)
  "Close a server socket."
  (error "Not implemented yet."))
