;;; UNETWORK library
;;; Condition definitions
;;;
;;; Copyright (C) 2003-2004  Matthieu Villeneuve (matthieu.villeneuve@free.fr)
;;;
;;; The author grants you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(in-package :unetwork)

(define-condition network-error (simple-error)
  ())

(define-condition connection-error (network-error)
  ((host :initarg :host :reader connection-error-host))
  (:report (lambda (condition stream)
             (format stream "Error connecting to host ~A"
                     (connection-error-host condition)))))

(define-condition socket-error (network-error)
  ((socket :initarg :socket :reader socket-error-socket))
  (:report (lambda (condition stream)
             (format stream "I/O error with socket ~S"
                     (socket-error-socket condition)))))

(define-condition unknown-host-error (network-error)
  ((host :initarg :host :reader unknown-host-error-host))
  (:report (lambda (condition stream)
             (format stream "Unknown host ~A"
                     (unknown-host-error-host condition)))))

(define-condition authenticate-error (network-error)
  ((user :initarg :user :reader authenticate-error-user))
  (:report (lambda (condition stream)
             (format stream "Authentication error for user ~A"
                     (authenticate-error-user condition)))))

(define-condition protocol-error (network-error)
  ((protocol :initarg :protocol :reader protocol-error-protocol)
   (command :initarg :command :reader protocol-error-command))
  (:report (lambda (condition stream)
             (format stream "Protocol ~A error on command ~A"
                     (protocol-error-protocol condition)
                     (protocol-error-command condition)))))
