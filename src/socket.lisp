;;; UNETWORK library
;;; Basic class definitions
;;;
;;; Copyright (C) 2003-2004  Matthieu Villeneuve (matthieu.villeneuve@free.fr)
;;;
;;; The author grants you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(in-package :unetwork)

(defclass socket ()
  ((sock :initarg :sock :reader socket-sock)
   (stream :initarg :stream :reader socket-stream)))

(defclass connection ()
  ((socket :initarg :socket :reader connection-socket)
   (status :initarg :status :reader connection-status)
   (properties :initarg :properties :reader connection-properties)))

(defun open-connection (uri)
  "Open a connection to a resource designated by an URI. Returns
a CONNECTION object."
  (let ((protocol (uri-scheme uri)))
    (open-protocol-connection uri protocol)))

(defgeneric open-protocol-connection (uri protocol))

(defgeneric close-connection (connection))
