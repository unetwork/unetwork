;;; UNETWORK library
;;; Utility functions
;;;
;;; Copyright (C) 2003-2004  Matthieu Villeneuve (matthieu.villeneuve@free.fr)
;;;
;;; The author grants you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(in-package :unetwork)

(defmacro with-gensyms (vars &body body)
  "Bind variables to gensyms and execute the BODY forms in an
implicit PROGN."
  `(let ,(mapcar (lambda (var)
                   `(,var (gensym)))
                 vars)
    ,@body))

(defun make-keyword (name)
  "Make a keyword with given name. Attempts to respect the current
readtable case."
  (intern (case (readtable-case *readtable*)
            (:upcase (string-upcase name))
            (:downcase (string-downcase name))
            (t name))
          :keyword))

(defun split-string (string &optional (separator #\Space))
  "Split a string into tokens, according to the specified separator."
  (loop for i = 0 then (1+ j)
        as j = (position separator string :start i)
        collect (subseq string i j)
        until (null j)))

(defun trim-line (line)
  "Return a copy of LINE, without the ending \r char (if any)."
  (cond ((null line) nil)
        ((zerop (length line)) (copy-seq ""))
        (t (let ((last-char (aref line (1- (length line)))))
             (if (char= last-char #\Return)
                 (subseq line 0 (1- (length line)))
                 (copy-seq line))))))

(defun read-properties (stream)
  "Read RFC822-formatted properties from a stream. Returns an alist
in which property names are keywords, formed using MAKE-KEYWORD."
  (loop with last-property = nil
        as line = (trim-line (read-line stream nil nil))
        until (or (null line) (zerop (length line)))
        as colon-index = (position #\: line)
        if (member (aref line 0) '(#\Space #\Tab))
        do (setf (cdr last-property)
                 (concatenate 'string (cdr last-property) line))
        else 
        do (let ((name (subseq line 0 colon-index))
                 (value (subseq line (+ colon-index 2))))
             (setf last-property (cons (make-keyword name) value)))
        collect last-property))

(defun read-dot-terminated-text (stream)
  "Read text from a character stream. Reading is done line by line.
Reading ends when encountering a line that only contains a dot, that
line is discarded. Returns a string."
  (with-output-to-string (result)
    (loop as line = (trim-line (read-line stream nil nil))
          until (or (null line) (string= line "."))
          do (format result "~A~%" line))))

(defun handle-simple-command (protocol socket command params
                              &key ok-status (ignore-status nil))
  "Send a command (COMMAND followed by the PARAMS separated by spaces)
to the socket and read a response line (a status word/number followed
by optional information). Returns two values: status and info.
Discards any line starting with status among the IGNORE-STATUS list.
Throws PROTOCOL-ERROR if the status is not among the OK-STATUS list."
  (let ((stream (socket-stream socket)))
    (format stream "~A~{ ~A~}~%" command params)
    (finish-output stream)
    (loop as line = (trim-line (read-line stream))
          as space-index = (position #\Space line)
          as status = (if (null space-index)
                          nil
                          (subseq line 0 space-index))
          as info = (if (null space-index)
                        line
                        (subseq line (1+ space-index)))
          when (member status ok-status :test #'string=)
          do (return (values status info))
          unless (member status ignore-status :test #'string=)
          do (error 'protocol-error
                    :protocol protocol :command command))))
