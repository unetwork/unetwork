;;; UNETWORK library
;;; URL handling
;;;
;;; Copyright (C) 2003-2004  Matthieu Villeneuve (matthieu.villeneuve@free.fr)
;;;
;;; The author grants you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(in-package :unetwork)

(unless (boundp '+url-decoded-chars+)
  (defconstant +url-decoded-chars+
    #(#\$ #\& #\+ #\, #\/ #\: #\; #\= #\? #\@)))

(unless (boundp '+url-encoded-chars+)
  (defconstant +url-encoded-chars+
    #("%24" "%26" "%2B" "%2C" "%2F" "%3A" "%3B" "%3D" "%3F" "%40")))

(defun url-encode (string)
  "URL-encodes a string."
  (with-output-to-string (stream)
    (loop for char across string
          as special-char-pos = (position char +url-decoded-chars+)
          do (format stream "~A"
                     (if (null special-char-pos)
                         char
                         (aref +url-encoded-chars+ special-char-pos))))))
