;;; UNETWORK library
;;; HTTP protocol
;;;
;;; Copyright (C) 2003-2004  Matthieu Villeneuve (matthieu.villeneuve@free.fr)
;;;
;;; The author grants you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(in-package :unetwork)

(unless (boundp '+http-default-port+)
  (defconstant +http-default-port+ 80))

(unless (boundp '+http-default-path+)
  (defconstant +http-default-path+ "/"))

(defgeneric http-get (uri)
  (:documentation "Get a resource designated by an URI. Returns two
values: the resource as an instance of the DOCUMENT class, and
the request response code as an integer."))

(defmethod http-get ((uri string))
  (http-get (parse-uri uri)))

(defmethod http-get ((uri uri))
  (http-perform-request uri "GET"))

(defun http-open-connection (uri method &key (request-headers '()))
  "Open a connection to a HTTP server, and request the resource
designated by URI using the given method (usually GET). Returns
three values: the response code as an integer, the resource
properties (headers) as an assoc list, and the connection socket."
  (assert (eq (uri-scheme uri) :http))
  (let* ((socket (open-socket (uri-host uri)
                              (or (uri-port uri) +http-default-port+)))
         (stream (socket-stream socket)))
    (format stream "~A ~A~@[?~A~] HTTP/1.0~%"
            method
            (or (uri-path uri) +http-default-path+)
            (uri-query uri))
    (format stream "Host: ~A~%" (uri-host uri))
    (loop for header in request-headers
          do (format stream "~A: ~A~%" (car header) (cdr header)))
    (format stream "~%")
    (finish-output stream)
    (let* ((response-code (http-read-response socket))
           (properties (read-properties stream)))
      (values response-code properties socket))))

(defun http-perform-request (uri method &key (request-headers '()))
  (multiple-value-bind (response-code properties socket)
      (http-open-connection uri method :request-headers request-headers)
    (unwind-protect
         (let ((content (http-read-content socket)))
           (values (make-instance 'document
                                  :text content
                                  :properties properties)
                   response-code))
      (close-socket socket))))

(defun http-read-response (socket)
  (let* ((stream (socket-stream socket))
         (response (trim-line (read-line stream nil nil))))
    (when (null response)
      (error 'socket-error :socket socket))
    (let ((response-tokens (split-string response #\Space)))
      (parse-integer (second response-tokens)))))

(defun http-read-content (socket)
  (let* ((stream (socket-stream socket))
         (element-type (stream-element-type stream))
         (reader (if (subtypep element-type 'integer)
                     #'read-byte
                     #'read-char)))
    (loop with content = (make-array '(0)
                                     :element-type element-type
                                     :fill-pointer 0 :adjustable t)
          as byte = (funcall reader stream nil nil)
          until (null byte)
          do (vector-push-extend byte content)
          finally (return content))))

(defclass http-connection (connection)
  ())

(defmethod open-protocol-connection ((uri uri) (protocol (eql :http)))
  (multiple-value-bind (response-code properties socket)
      (http-open-connection uri "GET")
    (make-instance 'http-connection
                   :socket socket
                   :status response-code
                   :properties properties)))

(defmethod close-connection ((connection http-connection))
  (close-socket (connection-socket connection)))
