;;; UNETWORK library
;;; SMTP protocol
;;;
;;; Copyright (C) 2003-2004  Matthieu Villeneuve (matthieu.villeneuve@free.fr)
;;;
;;; The author grants you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(in-package :unetwork)

(unless (boundp '+smtp-default-port+)
  (defconstant +smtp-default-port+ 25))

(defun smtp-open-connection (server &optional (port +smtp-default-port+))
  "Open a connection to a SMTP server. Returns the connection socket."
  (let* ((socket (open-socket server port :type :text))
         (stream (socket-stream socket)))
    (read-line stream)
    socket))

(defun smtp-close-connection (socket)
  "Close a connection to a SMTP server."
  (let ((stream (socket-stream socket)))
    (format stream "QUIT~%")
    (finish-output stream))
  (close-socket socket))

(defun smtp-send-mail (socket sender recipients subject data)
  "Send a mail from SENDER (an email address) to RECIPIENTS (a list
of conses (type . address), type being :TO, :CC or :BCC.)."
  (let ((stream (socket-stream socket)))
    (smtp-send-command socket "HELO"
                       (list "localhost")
                       :expect '("250" "220"))
    (smtp-send-command socket "MAIL FROM:"
                       (list (smtp-format-address sender))
                       :expect '("250"))
    (loop for (type . recipient) in recipients
          do (smtp-send-command socket "RCPT TO:"
                                (list (smtp-format-address recipient))
                                :expect '("250" "550")))
    (smtp-send-command socket "DATA" '() :expect '("354"))
    (format stream "Subject: ~A~%" subject)
    (format stream "From: ~A~%" sender)
    (loop for (type . recipient) in recipients
          when (member type '(:to :cc))
          do (format stream "~A: ~A~%"
                     (if (eq type :to) "To" "Cc")
                     recipient))
    (format stream "~%")
    (format stream data)
    (loop for char in '(#\Return #\Linefeed #\. #\Return #\Linefeed)
          do (write-char char stream))
    (finish-output stream)
    (read-line stream)
    t))

(defmacro with-smtp-connection ((socket server
                                 &optional (port +smtp-default-port+))
                                &body body)
  `(let ((,socket (smtp-open-connection ,server ,port)))
     (unwind-protect
          (progn ,@body)
       (smtp-close-connection ,socket))))

(defun smtp-send-command (socket command params &key expect)
  (handle-simple-command "SMTP" socket command params
                         :ok-status expect :ignore-status '("220")))

(defun smtp-format-address (address)
  (format nil "<~A>" address))
