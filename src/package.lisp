;;; UNETWORK library
;;; Package definition
;;;
;;; Copyright (C) 2003-2004  Matthieu Villeneuve (matthieu.villeneuve@free.fr)
;;;
;;; The author grants you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(defpackage :unetwork
  (:use :common-lisp :puri)
  (:nicknames :unet)
  (:export #:network-error
           #:connection-error
           #:socket-error
           #:unknown-host-error
           #:authenticate-error
           #:protocol-error

           #:socket
           #:socket-sock
           #:socket-stream

           #:connection
           #:connection-socket
           #:connection-status
           #:connection-properties
           #:open-connection

           #:document
           #:document-properties
           #:document-text

           #:resolve-host-ip
           #:socket
           #:open-socket
           #:close-socket
           #:open-server-socket
           #:close-server-socket
           #:server-socket-accept

           #:uri
           #:uri-scheme
           #:uri-host
           #:uri-port
           #:uri-path
           #:uri-query
           #:uri-fragment
           #:uri-plist
           #:uri-authority
           #:copy-uri
           #:parse-uri

           #:url-encode

           #:with-uri-input-stream

           #:+http-default-port+
           #:http-ensure-url-port-path
           #:http-get

           #:+pop3-default-port+
           #:pop3-open-connection
           #:pop3-close-connection
           #:pop3-authenticate
           #:pop3-get-mail-ids
           #:pop3-get-mail
           #:pop3-delete-mail
           #:with-pop3-connection

           #:+smtp-default-port+
           #:smtp-open-connection
           #:smtp-close-connection
           #:smtp-send-mail
           #:with-smtp-connection

           #:+nntp-default-port+
           #:nntp-open-connection
           #:nntp-close-connection
           #:nntp-get-groups
           #:nntp-get-group-info
           #:nntp-get-article
           #:with-nntp-connection))
