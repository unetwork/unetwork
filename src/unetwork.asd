;;; UNETWORK library
;;; ASDF system definition
;;;
;;; Copyright (C) 2003-2004  Matthieu Villeneuve (matthieu.villeneuve@free.fr)
;;;
;;; The author grants you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(defpackage #:unetwork-system
  (:use #:asdf #:common-lisp))

(in-package #:unetwork-system)

(defsystem unetwork-base
  :depends-on (:puri)
  :components ((:file "package")
               (:file "errors" :depends-on ("package"))
               (:file "utilities" :depends-on ("package"))
               (:file "document" :depends-on ("package" "utilities"))
               (:file "socket" :depends-on ("package" "utilities"))))

(defsystem unetwork-clisp
  :depends-on (:unetwork-base)
  :components ((:file "base-clisp")))

(defsystem unetwork-cmu
  :depends-on (:unetwork-base)
  :components ((:file "base-cmu")))

(defsystem unetwork-sbcl
  :depends-on (:sb-bsd-sockets :unetwork-base)
  :components ((:file "base-sbcl")))

(defsystem unetwork
  :name "unetwork"
  :depends-on (#+clisp :unetwork-clisp
               #+cmu :unetwork-cmu
               #+sbcl :unetwork-sbcl)
  :components ((:file "url")
               (:file "uri-streams")
               (:file "http" :depends-on ("uri-streams"))
               (:file "pop3")
               (:file "smtp")
               (:file "nntp")))
