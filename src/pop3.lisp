;;; UNETWORK library
;;; POP3 protocol
;;;
;;; Copyright (C) 2003-2004  Matthieu Villeneuve (matthieu.villeneuve@free.fr)
;;;
;;; The author grants you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(in-package :unetwork)

(unless (boundp '+pop3-default-port+)
  (defconstant +pop3-default-port+  110))

(unless (boundp '+pop3-status-ok+)
  (defconstant +pop3-status-ok+ "+OK"))

(unless (boundp '+pop3-status-error+)
  (defconstant +pop3-status-error+ "-ERR"))

(defun pop3-open-connection (server &optional (port +pop3-default-port+))
  "Open a connection to a POP3 server. Returns the connection socket."
  (let* ((socket (open-socket server port :type :text))
         (stream (socket-stream socket)))
    (loop as line = (trim-line (read-line stream nil nil))
          until (or (null line)
                    (let ((line-tokens (split-string line #\Space)))
                      (string= (first line-tokens) +pop3-status-ok+))))
    socket))

(defun pop3-close-connection (socket)
  "Close a connection to a POP3 server."
  (pop3-handle-command socket "QUIT"))

(defun pop3-authenticate (socket user password)
  "Sends authentication information to the server."
  (pop3-handle-command socket "USER" user)
  (pop3-handle-command socket "PASS" password))

(defun pop3-get-mail-ids (socket)
  "Returns the list of mail IDs in the mailbox."
  (pop3-handle-command socket "LIST")
  (let ((stream (socket-stream socket)))
    (loop for line = (trim-line (read-line stream nil nil))
          until (string= line ".") 
          as line-tokens = (split-string line #\Space)
          collect (first line-tokens))))

(defun pop3-get-mail (socket mail-id)
  "Gets a mail from the mailbox. Returns a DOCUMENT object."
  (pop3-handle-command socket "RETR" mail-id)
  (let ((document (make-instance 'document))
        (stream (socket-stream socket)))
    (document-read-properties document stream)
    (document-read-dot-terminated-text document stream)
    document))

(defun pop3-delete-mail (socket mail-id)
  "Deletes a mail from the mailbox."
  (pop3-handle-command socket "DELE" mail-id))

(defmacro with-pop3-connection ((socket user password server
                                 &optional (port +pop3-default-port+))
                                &body body)
  "Handle a POP3 session. Opens a connection to a server, binds
SOCKET to the connection, and executes BODY in an implicit PROGN."
  `(let ((,socket (pop3-open-connection ,server ,port)))
     (unwind-protect
          (progn
            (pop3-authenticate ,socket ,user ,password)
            ,@body
            (ignore-errors (pop3-close-connection ,socket)))
       (close-socket ,socket))))

(defun pop3-handle-command (socket command &rest params)
  (handle-simple-command "POP3" socket command params
                         :ok-status (list +pop3-status-ok+)))
