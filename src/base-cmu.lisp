;;; UNETWORK library
;;; Basic features for the CMUCL implementation
;;;
;;; Copyright (C) 2003-2004  Matthieu Villeneuve (matthieu.villeneuve@free.fr)
;;;
;;; The author grants you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(in-package :unetwork)

(defun resolve-host-ip (hostname)
  "Return the IP address of a host."
  (let ((host (ext:lookup-host-entry hostname)))
    (when (null host)
      (error 'unknown-host-error :host hostname))
    (first (ext:host-entry-addr-list host))))

(defun open-socket (host port &key (type :text))
  "Open a socket on specified host and port. Keyword argument TYPE
can be either :TEXT or :BINARY (defaults to :TEXT)."
  (let ((type (translate-type type)))
    (handler-case
        (let* ((sock (ext:connect-to-inet-socket host port))
               (stream (sys:make-fd-stream 
                        sock
                        :input t :output t
                        :element-type type)))
          (make-instance 'socket :sock sock :stream stream))
    (simple-error () (error 'connection-error :host host)))))
  
(defun close-socket (socket)
  "Close a socket."
  (ext:close-socket (socket-sock socket)))

(defun open-server-socket (port)
  "Open a server socket on localhost on specified port."
  (ext:create-inet-listener port :stream :reuse-address t))

(defun server-socket-accept (server-socket &key (type :text) timeout)
  "Accept a connection on a server socket. Return the resulting socket.
Keyword argument TYPE can be either :TEXT or :BINARY (defaults to :TEXT)."
  (let ((type (translate-type type)))
    (when (sys:wait-until-fd-usable server-socket :input timeout)
      (let* ((sock (ext:accept-tcp-connection server-socket))
             (stream (sys:make-fd-stream sock
                                         :input t :output t
                                         :element-type type)))
        (make-instance 'socket :sock sock :stream stream)))))

(defun close-server-socket (server-socket)
  "Close a server socket."
  (unix:unix-close server-socket))

(defun translate-type (type)
  (ecase type
    (:text 'base-char)
    (:binary '(unsigned-byte 8))))
