;;; UNETWORK library
;;; Basic document object
;;;
;;; Copyright (C) 2003-2004  Matthieu Villeneuve (matthieu.villeneuve@free.fr)
;;;
;;; The author grants you the rights to distribute
;;; and use this software as governed by the terms
;;; of the Lisp Lesser GNU Public License
;;; (http://opensource.franz.com/preamble.html),
;;; known as the LLGPL.


(in-package :unetwork)

(defclass document ()
  ((properties :initarg :properties :reader document-properties)
   (text :initarg :text :reader document-text)))

(defmethod document-read-properties ((document document) stream)
  "Read properties from a character stream. Each property must be
formatted according to RFC 822. Reading stops when encoutering an
empty line. Return the properties as an association list."
  (setf (slot-value document 'properties)
        (read-properties stream)))

(defmethod document-read-dot-terminated-text ((document document) stream)
  "Read text from a character stream. Finishes when encounters
a line containing a single dot."
  (setf (slot-value document 'text)
        (read-dot-terminated-text stream)))
